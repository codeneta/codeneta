module.exports = {
  siteMetadata: {
    title: 'CoDeNETa',
    editBaseUrl: 'https://gitlab.com/codeneta/codeneta/-/blob/master'
  },
  plugins: [
    '@confluenza/gatsby-theme-confluenza',
    'gatsby-plugin-emotion',
    'gatsby-plugin-catch-links',
    'gatsby-plugin-root-import'
  ]
}
