# CoDeNETa - Co-owned Decentralized Networks

Decentralized networks bring many promises to the community: preserving content, better use of the fast fiber connection (bandwidth), self-sovereign identity paving the way to private, secure communications and data exchange. Yet, there is a problem.

Decentralized networks need physical nodes. Hosting virtual nodes in the cloud is not what we would call decentralization: the data is still in one place, network traffic is consolidated, and the infrastructure co-ownership does not exist.

With CoDeNETa we want to change that by providing the ultimate experience of decentralization right to people's homes. CoDeNETa is a network of physical, connected devices, enabling multiple decentralized ecosystems to coexist on a reliable, physically distributed network. To achieve its objectives, CoDeNETa provides an open software platform that ensures connectivity and interoperability, and thus enabling both existing and new decentralized platforms to find their home and reach the actual users.

[Read the white paper](https://docs.codeneta.net/white-paper).

[Visit CoDeNETa home page](https://codeneta.net).
