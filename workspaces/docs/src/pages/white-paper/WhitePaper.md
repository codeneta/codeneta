---
path: /white-paper
title: CoDeNETa White Paper
tag: white-paper
---

Decentralized networks bring many promises to the community: preserving content, better use of the fast fiber connection (bandwidth), self-sovereign identity paving the way to private, secure communications and data exchange. Yet, there is a problem.

Decentralized networks need physical nodes. Hosting virtual nodes in the cloud is not what we would call decentralization: the data is still in one place, network traffic is consolidated, and the infrastructure co-ownership does not exist.

With CoDeNETa we want to change that by providing the ultimate experience of decentralization right to people's homes. CoDeNETa is a network of physical, connected devices, enabling multiple decentralized ecosystems to coexist on a reliable, physically distributed network. To achieve its objectives, CoDeNETa provides an open software platform that ensures connectivity and interoperability, and thus enabling both existing and new decentralized platforms to find their home and reach the actual users.

## Power of decentralization

> Decentralization has been lying dormant for thousands of years. But the advent of the Internet has unleashed this force, knocking down traditional business, altering entire industries, affecting how we relate to each other, and influencing world politics. The absence of structure, leadership, and formal organization, once considered a weakness, has become a major asset. Seemingly, chaotic groups have challenged and defeated established institutions. The rules of the game have changed.

The above quotation is not from some science-fiction novel. It is from a book written by _Ori Brafman and Rod A. Beckstrom_ in 2006 with a title: _The Starfish and the Spider_. A book about what happens when there is no one in charge. By providing a convincing historical evidence, the authors show how apparent chaos and lack of control turn into powerful tool separating these who survive from those who extinct; from these who win from those who lose. Just as in the story about Apaches, who unlike the Aztecs and the Incas, did not build a single pyramid, nor paved a single highway, yet they defeated the Spanish invaders by the way they organized as a society. The Apaches persevered because they were decentralized.

The authors of _The Starfish and the Spider_ also refer to more recent facts: the history of Napster, Kazaa, and eMule and how they revolutionized the recording industry. Or how Skype put an end to expensive long-distance calls. Or how Wikipedia changed the access to knowledge once for all. In all these examples, and these are just a few the history knows about, decentralization was doing something good for the society. Why then today, in 2020, the same Internet that _unleashed this force_, itself evolved to a set of largely centralized ecosystems? Did we just turned into a spider, the same way the music industry did when moving from the world of individual musicians to the world of big record labels, just to become a starfish again with the end of the second millenium?

### The power of data

When the Internet has started only few had access to it. This changed quickly. So quickly, that we did not have time to do things right. As a community and as a modern society.

With more and more users connected and seeking services, we did not have to wait long for a response from the market. Search engines and social networks started to appear. And they were all free...not for long.

Soon it became clear that the ability to learn about people's habits, interests, and anything related to them is an asset that the Internet industry could not ignore. The pioneers of social media quickly become powerful players, providing the users with free email accounts, file storage, messaging apps, discussion forums, media exchange portals, and more. And they rarely asked for money. They did not have to: the user became their most valuable asset.

Fortunately, the customers are becoming increasingly more aware of the importance of protecting their digital content and it is becoming clear that the current centralized model has came to an end. We need to turn back to what the authors of the _The Starfish and the Spider_ book remind us had always worked to our advantage: _decentralization_.

## Decentralizing the Internet again

After initial success of P2P networks, they seemed to fade away from the foreground: often forming an important part of the architecture, but no longer directly visible to end users. This has changed a bit with the advent of distributed ledgers and crypto-currencies, but even after undebatable success of networks like Ethereum, P2P networks still seem to remain far from the mainstream use.

Decentralized networks are well-research, they may bring direct advantage to the community, yet they seem to lose the competition against powerful centralized establishments. This should not take us by surprise. As indicated by Brafman and Beckstrom: _as industries become decentralized, overall profits decrease_. No wonder that decentralization, or giving up on control, is not on the closest roadmap of the mighty.

But there is hope. Firstly, the users start to realize that they do not really control the data that are theirs, that their digital identity can be taken away from them on short notice, and reminded that _free_ does not exist. Secondly, we observe that more and more professionals start (re)building decentralized solutions, platforms, and ecosystems. [IPFS](http://IPFS.io/), [Scuttlebutt](https://scuttlebutt.nz), and [dat://](https://dat.foundation) are great examples of community efforts to create new, modern decentralized ecosystems. Not to forget the [Decentralized Identifier Working Group](https://www.w3.org/2019/did-wg/) who lead the effort of the W3C Recommendation for [Decentralized Identifiers (DIDs)](https://www.w3.org/TR/did-core/) with the support of the [Rebooting the Web of Trust community](https://www.weboftrust.info).

We are also happy to observe new and creative ideas for monetization on distributed networks. Keeping in mind what we state above - _as industries become decentralized, overall profits decrease_ - new community oriented business models need to be invented and tried out, and public funding need to be made available to support communities working for the future of the Internet and for the true advantage of both individuals and whole society.

In the following sections we show how CoDeNETa is trying to contribute to the future of the Internet. Let's start with an observation.

## Mobile is not enough!

Smartphones are currently more ubiquitous than desktop computers. Unfortunately, many state-of-the-art P2P technologies rely on assumptions that do not hold on mobile devices, such as unbounded bandwidth, low latency, abundant cheap storage, and persistent connectivity. Decentralization technologies are a cornerstone for achieving digital sovereignty, but energy budget restrictions make mobile devices unsuitable to operate as full nodes of a peer to peer network. The fact that most users still have at least one desktop or a laptop computer at home does not help since those are often off or disconnected for most of the time, and even when online, hardly reachable behind NAT (Network address translation) devices and firewalls. [Figure 1](#figure-1) below illustrates the problem.

<a id="figure-1"></a> 
<div class="flex-wrap responsive">
<div class="bordered-content-600 responsive-image">
  <img alt="problem with decentralized networks today" src="assets/WhitePaper-assets/Problem.png"/>
</div>
<p class="figure-title"><b>Figure 1</b> There is a problem...</p>
</div>

Externalizing the persistent connectivity and gossip protocols to always-connected hardware solves this problem. Some initiatives lean on centralized cloud services to operate "P2P nodes" in a centralized fashion, and then charging for access to these nodes. This however introduces a new problem. Users tend to favor these highly available reliable nodes, reducing the incentive to host their own nodes. This leads to a degeneration of the P2P network into highly centralized architecture.

CoDeNETa, however, is more than just a piece of connected hardware...

## Introducing CoDeNETa

CoDeNETa, or Co-owned Decentralized Networks, is an open platform to help building decentralized infrastructure. CoDeNETa has three equally important aspects: (1) a physical device to help building actual physically distributed network, (2) Connectivity hub, to provide secure and stable connection with other distributed nodes, (3) Self-Sovereign Identity (SSI) provider to enable consistent user identity across different distributed apps.

Let's elaborate a bit more on these three points below.

### A physical device

Matt Ober, CoFounder and CTO of [Pinata](http://pinata.cloud/), in his blog post [The IPFS Gateway Problem](https://medium.com/pinata/the-ipfs-gateway-problem-64bbe7eb8170) observes that:

> By utilizing a gateway to retrieve content on IPFS, we’re recreating the same Web2 architecture that IPFS aims to disrupt.

He also indicates that:

> As a gateway increases capabilities, it’s more likely that the gateway will be utilized by the IPFS network.

[The IPFS Gateway Problem](https://medium.com/pinata/the-ipfs-gateway-problem-64bbe7eb8170) focuses on a popular IPFS decentralized network, but the observations can easily be extended to any other decentralized network.

The problem is the following: when most of the nodes in the network are highly unstable, meaning their availability and robustness is limited, the whole network suffers. This is a direct consequence of the fact that most of the decentralized discovery protocols favor stable nodes. The same holds when the nodes are available and robust, but their number is limited or they are collocated. When nodes are collocated in one data center, they effectively can be seen as a single, super-powerful node, yet limited in the available bandwidth (collocation), and usually remaining under control of a third party.

If such a super-powerful node can deliver without counting on other nodes (in case of a cluster, on other nodes not collocated in the same cluster), then all network traffic starts going to that node (cluster) only. In other words, the character of the network changes, decentralization is stopped, the network becomes centralized again. Collocating nodes in one data center has also negative impact on content replication and fault-tolerance as a failure of the given data center effectively removes access to the whole cluster of nodes operating in this data center.

To summarize, in order for the distributed network to remain fault-tolerant, replicating content, bandwidth-efficient, and remain under the control of individuals, the network needs to be both available, and physically distributed.

For this reason, although we do not intend to build hardware on its own, our ambition is to provide the end users with an ultimate and complete experience of decentralization by offering a carefully selected combination of hardware and software modules, still allowing them to choose their own hardware solution and run CoDeNETa on it.

### Connectivity hub

We said that CoDeNETa is more than just a piece of connected hardware. It's also a software, and it starts with connectivity. Most of the existing decentralized apps assume connectivity, which becomes responsibility of the user. Unfortunately, if we assume that hole punching, port knocking, and other NAT traversal techniques are not beyond understanding of an average user, it is often beyond her concern. This is one of the reasons most of the users of the decentralized apps are technically savvy users.

CoDeNETa takes care for making sure that any web app running on a CoDeNETa device will be externally reachable by other nodes and also from web and mobile apps that need to securely exchange data with the given CoDeNETa device.

### Self-sovereign Identity

Having a connected network is a great enabler. Still, lots of existing and future decentralized apps will depend on user identity. Today, many decentralized apps are limited in this aspect. In some cases, for instance in [Patchwork](https://github.com/ssbc/patchwork), a user cannot reuse the identity when using the app from distinct devices, and in general, there is no sufficient interoperability between various decentralized apps meaning that an identity used e.g. in the mentioned Patchwork, cannot be easily used in [Cabal](https://cabal.chat).

In CoDeNETa we bet on Self-Sovereign Identity (SSI), an identity that is owned and controlled by the user. This is in contrast of many centralized proposals, where the identity is granted to a user by a third-party and as such can be taken away from her at any moment. With SSI, our users will be able to create their identity in their discretion, and only reveal what they want, thus maintaining privacy.

We closely observe the [Decentralized Identifiers (DIDs)](https://www.w3.org/TR/did-core/) proposal and our initial implementation is aligned with the [IPID DID Method](https://did-ipid.github.io/ipid-did-method/). Finally, we want our self-sovereign identity solution to be open and extendable so that different identity methods can co-exist.

## Summary

[Figure 2](#figure-2) below summarizes the three aspects of CoDeNETa.

<a id="figure-2"></a> 
<div class="flex-wrap responsive">
<div class="bordered-content-600 responsive-image">
  <img alt="CoDeNETa" src="assets/WhitePaper-assets/Solution.png"/>
</div>
<p class="figure-title"><b>Figure 2</b> CoDeNETa</p>
</div>

In this short article we presented CoDeNETa. We started with describing the problem and the limitations of the current centralized Internet. We indicated that decentralization may be a solution to many of those and we briefly enumerated the difficulties for the decentralized networks to become mainstream. We also have shown that the society becomes more and more aware of the sensitive nature of the data, level of control, and having pace of mind when sharing the data with other peers. We indicated that new, community-based economy is needed to support initiatives that could benefit both individuals and societies without exploiting them.

Finally, we introduced CoDeNETa, our contribution to building co-owned, decentralized networks, open to various decentralized ecosystems and platforms by providing physical devices to grow the reliable and stable decentralized infrastructure, warrantying strong and secure connectivity between peers and user devices, and finally by providing a uniform identity platform to increase interoperability between various decentralized ecosystems.

This document should not be treated as exhaustive, but rather as an introduction and encouragement. One area that we do not touch upon with more detail in this document is the design and easy of use. Yet, we think that good, catchy, supportive, and inclusive design is equally important as the reliability of the whole network. We want CoDeNETa to be open and accessible. Easy of use is also our top priority. We observe that in many other cases, installing today's decentralized apps, although seemingly simple for the users with strong technical background, can form an impenetrable barrier for regular users. In other cases, there is either no mobile app available, or it is not fully functional or buggy. At CoDeNETa we aim at solutions that are inclusive, functional, stylish, reliable, and of high quality.

Join us! Let's do it together!

<style scoped>
.scrollable {
  width: 100%;
  overflow-x: auto;
}
.flex-wrap {
  display:flex;
  flex-flow:column;
  justify-content:center;
  align-items: center;
}
.figure-title {
  font-size: 0.8em
}
.bordered-content-600 {
  width: 600px;
  border: 1px solid black;
}
.bordered-content-300 {
  width: 300px;
  border: 1px solid black;
}
@media (max-width: 650px) {
  .responsive {
    align-items: flex-start;
    width: 100%;
  }
  .responsive-image {
    width: 100%;
  }  
}
</style>
